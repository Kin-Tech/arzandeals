import Head from 'next/head';
import styles from '../../styles/pages/FoodDeals.module.css';
import Header from '../../components/Header';
import DealCard from '../../components/DealCard';
import Footer from '../../components/Footer';
export default function FoodDeals() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>
      <Header />
      <main className={styles.main}>
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
        <DealCard />
      </main>
      <Footer />
    </div>
  );
}
