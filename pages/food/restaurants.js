import Head from 'next/head';
import styles from '../../styles/pages/Restaurants.module.css';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Restaurant from '../../components/Restaurant';
export default function Restaurants() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>
      <Header />
      <main className={styles.main}>
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
        <Restaurant />
      </main>
      <Footer />
    </div>
  );
}
