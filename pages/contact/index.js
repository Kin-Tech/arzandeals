import Head from 'next/head';
import styles from '../../styles/pages/Contact.module.css';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
export default function Contact() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>
      <Header color="blue" />
      <main className={styles.main}>
        <h1 className={styles.heading}>CONTACT US</h1>
        <form className={styles.form}>
          <label className={styles.label}>
            <span className={styles.labelName}>NAME:</span>{' '}
            <input className={styles.input} id="name" type="text" />
          </label>
          <label className={styles.label}>
            <span className={styles.labelName}>EMAIL:</span>{' '}
            <input className={styles.input} id="email" type="email" />
          </label>
          <label className={styles.label}>
            <span className={styles.labelName}>QUERY:</span>{' '}
            <textarea className={styles.textarea}></textarea>
          </label>
          <button className={styles.submitbtn}>Submit</button>
        </form>
      </main>
      <Footer color="blue" />
    </div>
  );
}
