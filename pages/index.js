import Head from 'next/head';
import styles from '../styles/pages/Home.module.css';
import Image from 'next/image';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Link from 'next/link';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.Section}>
          <Image
            src="/LPfood.png"
            alt="home banner"
            objectFit="cover"
            layout="fill"
          />
          <div className={styles.food}>
            <div className={styles.text}>
              <h1 className={styles.heading}>Food Deals</h1>
              <p className={styles.para}>
                Find about all the latest deals and discounts on food
              </p>
            </div>
            <div className={styles.placeHolder}></div>
            <Link href="/food/deals">
              <a className={styles.btnLink}>
                <ArrowForwardIcon style={{ fontSize: 25 }} />
              </a>
            </Link>
          </div>
        </div>
        <div className={styles.Section}>
          <Image
            src="/LPcloth.png"
            alt="home banner"
            objectFit="cover"
            layout="fill"
          />

          <div className={styles.cloth}>
            <div className={styles.text}>
              <h1 className={styles.heading}>Cloth Sale</h1>
              <p className={styles.para}>
                Find about all the latest sale and discounts on cloth
              </p>
            </div>
            <div className={styles.placeHolder}></div>
            <Link href="/cloth/deals">
              <a className={styles.btnLink}>
                <ArrowForwardIcon style={{ fontSize: 25 }} />
              </a>
            </Link>
          </div>
        </div>
      </main>
    </div>
  );
}
