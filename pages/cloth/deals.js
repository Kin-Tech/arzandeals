import Head from 'next/head';
import styles from '../../styles/pages/ClothDeals.module.css';
import Image from 'next/image';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import SaleCard from '../../components/SaleCard';
export default function ClothDeals() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>
      <Header color="red" />
      <main className={styles.main}>
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
        <SaleCard />
      </main>
      <Footer color="red" />
    </div>
  );
}
