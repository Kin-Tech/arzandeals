import Head from 'next/head';
import styles from '../../styles/pages/ClothDeals.module.css';
import Image from 'next/image';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Brand from '../../components/Brand';
export default function Brands() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>
      <Header color="red" />
      <main className={styles.main}>
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
        <Brand />
      </main>
      <Footer color="red" />
    </div>
  );
}
