import Head from 'next/head';
import styles from '../../styles/pages/Login.module.css';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

export default function Login() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Arzan Deals</title>
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <link rel="icon" href="favicon/favicon.ico" />
      </Head>
      {/* <Header color="blue" /> */}
      <main className={styles.main}>
        <h1 className={styles.heading}>Log In</h1>
        <button className={styles.btnLogin1}>Login with Facebook</button>
        <button className={styles.btnLogin2}>Login with Google</button>

        <form className={styles.form}>
          <label className={styles.label}>
            <span className={styles.labelName}>EMAIL:</span>{' '}
            <input
              className={styles.input}
              id="email"
              type="email"
              placeholder="Required Field"
            />
          </label>
          <label className={styles.label}>
            <span className={styles.labelName}>PASSWORD:</span>{' '}
            <input
              className={styles.input}
              id="password"
              type="password"
              placeholder="Required Field"
            />
          </label>
          <button className={styles.submitbtn}>
            <ArrowForwardIcon style={{ fontSize: 20 }} />
          </button>
        </form>
        <div className={styles.signup}>
          <p>Don't Have Account? </p>
          <button className={styles.signupBtn}>Sign up</button>
        </div>
      </main>
      {/* <Footer color="blue" /> */}
    </div>
  );
}
