import React from 'react';
import styles from '../styles/components/Header.module.css';
import Link from 'next/link';
import SearchIcon from '@material-ui/icons/Search';
import PersonIcon from '@material-ui/icons/Person';
import classNames from 'classnames';

function Header({ color }) {
  const headerClass = classNames({
    [styles.header]: true,
    [styles.headerBlue]: color === 'blue',
    [styles.headerRed]: color === 'red',
  });

  const btnClass = classNames({
    [styles.submitBtn]: true,
    [styles.submitBtnBlue]: color === 'blue',
    [styles.submitBtnRed]: color === 'red',
  });

  return (
    <div className={headerClass}>
      <p className={styles.logo}>
        <Link href="/">
          <a className={styles.btnLink}><img className={styles.img} src='https://res.cloudinary.com/dshitqpca/image/upload/v1609419692/logos/AD-logo_duellq.png' alt='arzan deals logo'/></a>
        </Link>
      </p>

      <div className={styles.formBox}>
        <form className={styles.form}>
          <input
            type="text"
            className={styles.input}
            placeholder="Search Deals"
          />
          <button className={btnClass}>
            <SearchIcon style={{ fontSize: 28 }} />
          </button>
        </form>
      </div>
      <div className={styles.nav}>
        <ul className={styles.navList}>
          {color === 'red' ? (
            <li className={styles.navItem}>
              <Link href="/cloth/brands">
                <a className={styles.btnLink}>BRANDS</a>
              </Link>
            </li>
          ) : color === 'blue' ? null : (
            <li className={styles.navItem}>
              <Link href="/food/restaurants">
                <a className={styles.btnLink}>RESTAURANTS</a>
              </Link>
            </li>
          )}
          {/* <li className={styles.navItem}>
            <Link href="/food/restaurants">
              <a className={styles.btnLink}>RESTAURANTS</a>
            </Link>
          </li> */}
          <li className={styles.navItem}>
            <Link href="/contact">
              <a className={styles.btnLink}>CONTACT US</a>
            </Link>
          </li>
          <li className={styles.navItem}>
            <Link href="/account/login">
              <a className={styles.btnLink}>
                <PersonIcon style={{ fontSize: 18, marginTop: 2 }} />
              </a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Header;
