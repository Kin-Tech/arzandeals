import React from 'react';
import styles from '../styles/components/DealCard.module.css';
import Image from 'next/image';

function DealCard() {
  return (
    <div className={styles.card}>
      <div className={styles.img}>
        <img src="https://www.pandemictechnews.com/wp-content/uploads/2020/05/dominos1.png" style={{width: "100%", height: "100%", borderRadius: '10px'}} />
      </div>
      <div className={styles.text}>
        <p className={styles.name}>Dominos Pizza</p>
        <p className={styles.discount}>Up to 30%</p>
      </div>
      <button className={styles.cardbtn}>More info</button>
    </div>
  );
}

export default DealCard;
