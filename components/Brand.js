import styles from '../styles/components/Brand.module.css';
import Image from 'next/image';

function Brand() {
  return (
    <div className={styles.card}>
      <div className={styles.image}>
        <Image src="/sale.jpeg" objectFit="cover" layout="fill" />
      </div>
      <div className={styles.details}>
        <p className={styles.para1}>Up to 30%</p>
        <p className={styles.para2}>card Offer</p>
        <p className={styles.para3}>Expires in 1 week</p>
      </div>
    </div>
  );
}

export default Brand;
