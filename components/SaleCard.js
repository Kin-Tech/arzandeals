import React from 'react';
import styles from '../styles/components/SaleCard.module.css';
import Image from 'next/image';

function SaleCard() {
  return (
    <div className={styles.card}>
      <div className={styles.logo}>
        <Image src="/khaadi.jpg" width="140px" height="90px" />
      </div>
      <div className={styles.text}>
        {/* <p className={styles.name}>Dominos Pizza</p> */}
        <p className={styles.discount}>Up to 50%</p>
      </div>
      <button className={styles.cardbtn}>See Details</button>
    </div>
  );
}

export default SaleCard;
