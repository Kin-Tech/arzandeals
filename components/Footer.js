import React from 'react';
import styles from '../styles/components/Footer.module.css';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import classNames from 'classnames';

function Footer({ color }) {
  const footerClass = classNames({
    [styles.footer]: true,
    [styles.footerBlue]: color === 'blue',
    [styles.footerRed]: color === 'red',
  });

  return (
    <div className={footerClass}>
      <div className={styles.footNav}>
        <div className={styles.item}>
          <p className={styles.para1}>START A PROJECT</p>
          <p className={styles.para2}>We are ready for the challenge</p>
          <p>adobexd@mail.com</p>
        </div>

        <div className={styles.item}>
          <p className={styles.para1}>SAY HELLO</p>
          <p className={styles.para2}>497 Evergreen Rd. Roseville, CA 95673</p>
          <p>+44 345 678 903</p>
        </div>

        <div className={styles.item}>
          <p className={styles.para1}>JOBS</p>
          <p className={styles.para2}>We are always looking for talent</p>
          <p>adobexd@mail.com</p>
        </div>
      </div>
      <div className={styles.footIcons}>
        <InstagramIcon style={{ fontSize: 18 }} />
        <TwitterIcon style={{ fontSize: 18 }} />
        <FacebookIcon style={{ fontSize: 18 }} />
      </div>
    </div>
  );
}

export default Footer;
