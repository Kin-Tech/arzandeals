import React from 'react';
import styles from '../styles/components/Restaurant.module.css';
import Image from 'next/image';

function Restaurant() {
  return (
    <div className={styles.card}>
      <div className={styles.image}>
        <Image src="/pizza.jpg" objectFit="cover" layout="fill" />
      </div>
      <div className={styles.details}>
        <p className={styles.para1}>Flat 30%</p>
        <p className={styles.para2}>Brand Offer</p>
        <p className={styles.para3}>Expires in 1 week</p>
      </div>
    </div>
  );
}

export default Restaurant;
